package com.shop.factor.product.repository;

import com.shop.criterion.factor.repository.FactorVariableRepository;
import com.shop.factor.product.Product;

/**
 * 产品仓库资源
 *
 * @author JayEinstein
 */
public interface ProductRepository extends FactorVariableRepository<Product> {

    /**
     * 扣减库存
     *
     * @param productId 产品ID
     * @param num       扣减数量
     * @return
     */
    boolean deductionStock(Long productId, Integer num);

    /**
     * 增加库存
     *
     * @param productId 产品ID
     * @param num       增加的数量
     * @return
     */
    boolean increaseStock(Long productId, Integer num);
}
