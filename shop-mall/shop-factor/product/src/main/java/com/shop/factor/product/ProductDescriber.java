package com.shop.factor.product;

import com.shop.criterion.factor.FactorDescriber;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 产品描述
 * @author JayEinstein
 */
@Data
public class ProductDescriber extends FactorDescriber<Product> {

    /**
     * 产品编码
     */
    private String code;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 产品价格
     */
    private BigDecimal price;

    /**
     * 产品库存
     */
    private Integer stock;

    public ProductDescriber() {
        this(null);
    }

    public ProductDescriber(Object value) {
        super(value);
    }

}
