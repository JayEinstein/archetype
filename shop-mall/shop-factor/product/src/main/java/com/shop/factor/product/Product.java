package com.shop.factor.product;

import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.FactorObserver;
import com.shop.criterion.factor.PersistentFactor;
import com.shop.factor.product.repository.ProductRepository;
import com.shop.factor.product.subject.OutOfStockObserver;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 产品变量因素
 * @author JayEinstein
 */
@Data
public class Product extends PersistentFactor<ProductRepository> {

    /**
     * 产品编码
     */
    private String code;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 产品价格
     */
    private BigDecimal price;

    /**
     * 产品库存
     */
    private Integer stock;

    /**
     * 兼容@Data，默认私有构造方法
     */
    private Product() {
        super(null);
    }

    protected Product(ProductRepository persistentRepository) {
        super(persistentRepository);
    }

    /**
     * 扣减库存
     * @param num 扣减数量
     * @return
     */
    public boolean deductionStock(Integer num) {

        boolean res = factorRepository.deductionStock(id, num);
        if (!res) {
            return false;
        }

        /* 这里只是做个简单的没库存示范，在并发的情况下stock并不准确 */
        stock -= num;
        if (stock < 1) {
            this.notifyObserver(OutOfStockObserver.class);
        }

        return true;
    }

    /**
     * 增加库存
     * @param num 增加的数量
     * @return
     */
    public boolean increaseStock(Integer num) {
        boolean res = factorRepository.increaseStock(id, num);
        if (!res) {
            return false;
        }

        /* 这里只是做个简单的没库存示范，在并发的情况下stock并不准确 */
        stock += num;
        if (stock < 1) {
            this.notifyObserver(OutOfStockObserver.class);
        }

        return true;
    }

    @Override
    protected <F extends Factor> boolean addObserver(FactorObserver<F>... observers) {
        return super.addObserver(observers);
    }
}
