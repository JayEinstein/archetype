package com.shop.factor.product.subject;

import com.shop.criterion.factor.FactorObserver;
import com.shop.factor.product.Product;

/**
 * 没库存订阅
 * @author JayEinstein
 */
public interface OutOfStockObserver extends FactorObserver<Product> {}
