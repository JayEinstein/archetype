package com.shop.factor.product;

import com.shop.criterion.factor.AbstractFactorFactory;
import com.shop.factor.product.repository.ProductRepository;

/**
 * @author JayEinstein
 */
public class ProductFactory extends AbstractFactorFactory<Product> {

    private final ProductRepository productRepository;

    public ProductFactory(ProductRepository productRepository) {
        super(productRepository);
        this.productRepository = productRepository;
    }

    @Override
    protected Product instance() {
        Product product = new Product(productRepository);
        product.addObserver(getFactorObservers());
        return product;
    }

}
