package com.shop.factor.member;

import com.shop.criterion.factor.FactorDescriber;
import lombok.Data;

/**
 * @author JayEinstein
 */
@Data
public class MemberDescriber extends FactorDescriber<Member> {

    private String userName;

    public MemberDescriber() {
        this(null);
    }

    public MemberDescriber(Object value) {
        super(value);
    }

}
