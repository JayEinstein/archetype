package com.shop.factor.member;

import com.shop.criterion.factor.AbstractFactorFactory;
import com.shop.factor.member.respository.MemberRepository;

/**
 * @author JayEinstein
 */
public class MemberFactory extends AbstractFactorFactory<Member> {

    private final MemberRepository memberRepository;

    public MemberFactory(MemberRepository factorVariableRepository) {
        super(factorVariableRepository);
        this.memberRepository = factorVariableRepository;
    }

    @Override
    protected Member instance() {
        return new Member(memberRepository);
    }

}
