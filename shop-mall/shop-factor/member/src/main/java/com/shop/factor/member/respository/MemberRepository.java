package com.shop.factor.member.respository;

import com.shop.criterion.factor.repository.FactorVariableRepository;
import com.shop.factor.member.Member;

/**
 * @author JayEinstein
 */
public interface MemberRepository extends FactorVariableRepository<Member> {

}
