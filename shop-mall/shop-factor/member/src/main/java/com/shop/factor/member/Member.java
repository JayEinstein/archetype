package com.shop.factor.member;

import com.shop.criterion.factor.PersistentFactor;
import com.shop.factor.member.respository.MemberRepository;
import lombok.Data;

/**
 * 商城会员因子
 * @author JayEinstein
 */
@Data
public class Member extends PersistentFactor<MemberRepository> {

    private String userName;

    /**
     * 兼容@Data，默认私有构造方法
     */
    private Member() {
        super(null);
    }

    /**
     * 持久因子构建
     *
     * @param memberRepository 持久资源对象
     */
    protected Member(MemberRepository memberRepository) {
        super(memberRepository);
    }

}
