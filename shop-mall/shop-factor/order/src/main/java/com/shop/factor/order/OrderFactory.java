package com.shop.factor.order;

import com.shop.criterion.factor.AbstractFactorFactory;
import com.shop.factor.order.repository.OrderRepository;

/**
 * @author JayEinstein
 */
public class OrderFactory extends AbstractFactorFactory<Order> {

    private final OrderRepository orderRepository;

    public OrderFactory(OrderRepository factorVariableRepository) {
        super(factorVariableRepository);
        this.orderRepository = factorVariableRepository;
    }

    @Override
    protected Order instance() {
        Order order = new Order(orderRepository);
        order.addObserver(getFactorObservers());
        return order;
    }

    /**
     * 获得订单回响
     * @return 订单回响
     */
    public OrderEcho getOrderEcho() {
        OrderEcho orderEcho = new OrderEcho(this, orderRepository);
        orderEcho.addObserver(getFactorObservers());
        return orderEcho;
    }

}
