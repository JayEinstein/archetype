package com.shop.factor.order;

import com.shop.criterion.factor.EchoFactor;
import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.FactorObserver;
import com.shop.criterion.factor.SpreadContext;
import com.shop.factor.order.repository.OrderRepository;
import com.shop.factor.order.subject.OrderPaidObserver;

/**
 * @author JayEinstein
 */
public class OrderEcho extends EchoFactor<Order> {

    protected final OrderRepository orderRepository;

    protected OrderEcho(OrderFactory orderFactory, OrderRepository factorEcho) {
        super(orderFactory, factorEcho);
        this.orderRepository = factorEcho;
    }

    /**
     * 变为付款状态
     * @param orderId 订单ID
     * @return
     */
    public boolean toPaid(Long orderId) {
        boolean res = orderRepository.updateOrderStatus(orderId, OrderStatusEnum.PAID.name());
        if (res) {
            // 创建上下文
            SpreadContext<Order> spreadContext = new SpreadContext<>();
            // 放入缓存
            spreadContext.putFactor(() -> parentFactory.get(orderId));
            // 放入工厂
            spreadContext.putFactory(getParentClass(), parentFactory);
            // 执行通知
            notifyObserver(OrderPaidObserver.class, spreadContext);
        }
        return res;
    }

    /**
     * 变为取消状态
     * @param orderId 订单ID
     * @return
     */
    public boolean toCancel(Long orderId) {
        return orderRepository.updateOrderStatus(orderId, OrderStatusEnum.CANCEL.name());
    }

    /**
     * 变为退款状态
     * @param orderId 订单ID
     * @return
     */
    public boolean toRefund(Long orderId) {
        return orderRepository.updateOrderStatus(orderId, OrderStatusEnum.REFUND.name());
    }

    @Override
    public Class<Order> getParentClass() {
        return Order.class;
    }

    @Override
    protected <F extends Factor> boolean addObserver(FactorObserver<F>... observers) {
        return super.addObserver(observers);
    }
}
