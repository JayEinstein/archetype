package com.shop.factor.order;

/**
 * 订单状态枚举
 * @author JayEinstein
 */
public enum OrderStatusEnum {

    /**
     * 待支付
     */
    WAIT,
    /**
     * 已取消
     */
    CANCEL,
    /**
     * 已支付
     */
    PAID,
    /**
     * 退款
     */
    REFUND

}
