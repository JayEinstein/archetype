package com.shop.factor.order.subject;

import com.shop.criterion.factor.FactorObserver;
import com.shop.factor.order.Order;

/**
 * 订单支付订阅通知
 * @author JayEinstein
 */
public interface OrderPaidObserver extends FactorObserver<Order> {
}
