package com.shop.factor.order;

import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.FactorObserver;
import com.shop.criterion.factor.PersistentFactor;
import com.shop.factor.order.repository.OrderRepository;
import com.shop.factor.order.subject.OrderPaidObserver;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单因子对象
 * @author JayEinstein
 */
@Data
public class Order extends PersistentFactor<OrderRepository> {

    /**
     * 订单编码
     */
    private String orderCode;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品编码
     */
    private String code;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 产品价格
     */
    private BigDecimal price;

    /**
     * 订单状态
     */
    private OrderStatusEnum status;

    /**
     * 订单支付金额
     */
    private BigDecimal payAmount;

    /**
     * 兼容@Data，默认私有构造方法
     */
    private Order() {
        super(null);
    }

    /**
     * 持久因子构建
     *
     * @param orderRepository 持久资源对象
     */
    protected Order(OrderRepository orderRepository) {
        super(orderRepository);
    }

    @Override
    protected <F extends Factor> boolean addObserver(FactorObserver<F>... observers) {
        return super.addObserver(observers);
    }

    /**
     * 订单取消
     * @return 取消成功
     */
    public boolean toCancel() {
        return factorRepository.updateOrderStatus(id, OrderStatusEnum.CANCEL.name());
    }

    /**
     * 订单支付
     * @return 支付成功
     */
    public boolean toPaid() {
        boolean res = factorRepository.updateOrderStatus(id, OrderStatusEnum.PAID.name());
        // 改为订单状态为支付成功的时候调用一下订阅
        if (res) {
            notifyObserver(OrderPaidObserver.class);
        }
        return res;
    }

    /**
     * 订单退款
     * @return 退款成功
     */
    public boolean toRefund() {
        return factorRepository.updateOrderStatus(id, OrderStatusEnum.REFUND.name());
    }

}
