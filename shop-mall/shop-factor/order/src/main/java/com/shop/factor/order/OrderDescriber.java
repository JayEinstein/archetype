package com.shop.factor.order;

import com.shop.criterion.factor.FactorDescriber;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 订单描述
 * @author JayEinstein
 */
@Data
@Accessors(chain = true)
public class OrderDescriber extends FactorDescriber<Order> {

    /**
     * 订单编码
     */
    private String orderCode;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品编码
     */
    private String code;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 产品价格
     */
    private BigDecimal price;

    /**
     * 订单状态
     */
    private OrderStatusEnum status;

    /**
     * 订单支付金额
     */
    private BigDecimal payAmount;

    public OrderDescriber() {
        this(null);
    }

    public OrderDescriber(Object value) {
        super(value);
    }

}
