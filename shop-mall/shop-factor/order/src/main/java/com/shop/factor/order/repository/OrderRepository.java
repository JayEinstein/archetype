package com.shop.factor.order.repository;

import com.shop.criterion.factor.repository.FactorVariableRepository;
import com.shop.factor.order.Order;

/**
 * @author JayEinstein
 */
public interface OrderRepository extends FactorVariableRepository<Order> {

    /**
     * 修改订单状态
     * @param orderId 订单ID
     * @param status 订单状态 {@link com.shop.factor.order.OrderStatusEnum}
     * @return 修改成功
     */
    boolean updateOrderStatus(Long orderId, String status);

}
