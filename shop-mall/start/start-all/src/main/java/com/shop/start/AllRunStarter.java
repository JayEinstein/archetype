package com.shop.start;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 单体启动
 * @author JayEinstein
 */
@SpringBootApplication
@MapperScan("com.shop.mvc.dao.mapper")
@ComponentScan(basePackages = {
        "com.shop.mvc.behavior",
        "com.shop.mvc.component",
        "com.shop.factor.repo"
})
public class AllRunStarter {

    public static void main(String[] args) {
        SpringApplication.run(AllRunStarter.class, args);
    }

}