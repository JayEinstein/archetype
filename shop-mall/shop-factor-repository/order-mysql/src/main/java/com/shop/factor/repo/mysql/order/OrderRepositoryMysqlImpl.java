package com.shop.factor.repo.mysql.order;

import com.shop.criterion.factor.FactorDescriber;
import com.shop.criterion.tools.FactorUtil;
import com.shop.factor.order.Order;
import com.shop.factor.order.OrderDescriber;
import com.shop.factor.order.repository.OrderRepository;
import com.shop.mvc.dao.entity.OrderEntity;
import com.shop.mvc.dao.mapper.OrderEntityMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author JayEinstein
 */
@Repository
public class OrderRepositoryMysqlImpl implements OrderRepository {

    @Autowired
    private OrderEntityMapper orderEntityMapper;

    @Override
    public boolean updateOrderStatus(Long orderId, String status) {

        OrderEntity update = new OrderEntity();
        update.setId(orderId);
        update.setStatus(status);
        // 简单暴力的改状态示范
        return orderEntityMapper.updateById(update) > 0;
    }


    @Override
    public FactorDescriber<Order> getDescribe(Long factorId) {
        OrderEntity orderEntity = orderEntityMapper.selectById(factorId);
        return new OrderDescriber(orderEntity);
    }

    @Override
    public Long create(FactorDescriber<Order> createDescriber) {
        OrderEntity orderEntity = createDescriber.convert(OrderEntity.class);
        orderEntityMapper.insert(orderEntity);
        return orderEntity.getId();
    }

    @Override
    public boolean updateById(FactorDescriber<Order> updateDescriber) {
        OrderEntity orderEntity = updateDescriber.convert(OrderEntity.class);
        return orderEntityMapper.updateById(orderEntity) > 0;
    }
}
