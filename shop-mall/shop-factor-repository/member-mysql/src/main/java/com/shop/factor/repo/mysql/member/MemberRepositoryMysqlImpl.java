package com.shop.factor.repo.mysql.member;

import com.shop.criterion.factor.FactorDescriber;
import com.shop.factor.member.Member;
import com.shop.factor.member.MemberDescriber;
import com.shop.factor.member.respository.MemberRepository;
import com.shop.mvc.dao.entity.MemberEntity;
import com.shop.mvc.dao.mapper.MemberEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author JayEinstein
 */
@Repository
public class MemberRepositoryMysqlImpl implements MemberRepository {

    @Autowired
    private MemberEntityMapper memberEntityMapper;

    @Override
    public FactorDescriber<Member> getDescribe(Long factorId) {
        MemberEntity memberEntity = memberEntityMapper.selectById(factorId);
        return new MemberDescriber(memberEntity);
    }

    @Override
    public Long create(FactorDescriber<Member> createDescriber) {
        MemberEntity memberEntity = createDescriber.convert(MemberEntity.class);
        memberEntityMapper.insert(memberEntity);
        return memberEntity.getId();
    }

    @Override
    public boolean updateById(FactorDescriber<Member> updateDescriber) {
        MemberEntity memberEntity = updateDescriber.convert(MemberEntity.class);
        return memberEntityMapper.updateById(memberEntity) > 0;
    }
}
