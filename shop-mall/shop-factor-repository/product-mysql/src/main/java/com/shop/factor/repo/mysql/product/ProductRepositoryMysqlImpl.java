package com.shop.factor.repo.mysql.product;

import com.shop.criterion.factor.FactorDescriber;
import com.shop.factor.product.Product;
import com.shop.factor.product.ProductDescriber;
import com.shop.factor.product.repository.ProductRepository;
import com.shop.mvc.dao.entity.ProductEntity;
import com.shop.mvc.dao.mapper.ProductEntityMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 产品仓库持久化实现
 * @author JayEinstein
 */
@Repository
public class ProductRepositoryMysqlImpl implements ProductRepository {

    @Autowired
    private ProductEntityMapper productEntityMapper;

    @Override
    public boolean deductionStock(Long productId, Integer num) {
        return productEntityMapper.deductionStock(productId, num);
    }

    @Override
    public boolean increaseStock(Long productId, Integer num) {
        return productEntityMapper.increaseStock(productId, num);
    }

    @Override
    public FactorDescriber<Product> getDescribe(Long factorId) {
        ProductEntity productEntity = productEntityMapper.selectById(factorId);
        return new ProductDescriber(productEntity);
    }

    @Override
    public Long create(FactorDescriber<Product> createDescriber) {

        ProductEntity entity = new ProductEntity();
        BeanUtils.copyProperties(createDescriber, entity);
        productEntityMapper.insert(entity);

        return entity.getId();
    }

    @Override
    public boolean updateById(FactorDescriber<Product> updateDescriber) {

        ProductEntity entity = new ProductEntity();
        BeanUtils.copyProperties(updateDescriber, entity);
        return productEntityMapper.updateById(entity) > 1;

    }

}
