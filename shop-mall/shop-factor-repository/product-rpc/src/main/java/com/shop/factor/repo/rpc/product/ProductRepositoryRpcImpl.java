package com.shop.factor.repo.rpc.product;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.shop.criterion.factor.FactorDescriber;
import com.shop.factor.product.Product;
import com.shop.factor.product.ProductDescriber;
import com.shop.factor.product.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

/**
 * 产品仓库持久化实现
 * @author JayEinstein
 */
@Slf4j
@Repository
public class ProductRepositoryRpcImpl implements ProductRepository {

    @Override
    public boolean deductionStock(Long productId, Integer num) {
        throw new RuntimeException("接口未实现");
    }

    @Override
    public boolean increaseStock(Long productId, Integer num) {

        JSONObject jsonObject = JSON.parseObject("{}");

        throw new RuntimeException("接口未实现");
    }

    @Override
    public Long create(FactorDescriber<Product> describer) {
        throw new RuntimeException("接口未实现");
    }

    @Override
    public boolean updateById(FactorDescriber<Product> describer) {
        throw new RuntimeException("接口未实现");
    }

    @Override
    public FactorDescriber<Product> getDescribe(Long factorId) {

        HttpRequest httpRequest = HttpRequest.get("http://localhost:8081/shop/product/get?productId=" + factorId);
        try (HttpResponse response = httpRequest.execute()) {
            String body = response.body();

            log.info("从http获得资源，{}", body);

            return JSON.parseObject(body, ProductDescriber.class);
        } catch (Exception e) {
            throw e;
        }
    }
}
