package com.shop.criterion.factor;

import com.shop.criterion.factor.repository.FactorVariableRepository;
import com.shop.criterion.tools.FactorUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 因子工厂<br>
 * 1. 提供获得因子的方法。<br>
 * 2. 提供创建、修改、删除因子的方法。<br>
 * 3. 可以提供当前泛型下的各种{@link EchoFactor}。<br>
 * @author JayEinstein
 */
public abstract class AbstractFactorFactory<F extends Factor> implements FactorFactory<F> {

    /**
     * 因子资源接口。
     */
    private final FactorVariableRepository<F> factorVariableRepository;

    /**
     * 因子监听器
     */
    protected List<FactorObserver<F>> factorObservers;

    public AbstractFactorFactory(FactorVariableRepository<F> factorVariableRepository) {
        this.factorVariableRepository = factorVariableRepository;
    }

    public void addObserver(FactorObserver<F>... factorObservers) {
        if (factorObservers == null) {
            return;
        }
        if (this.factorObservers == null) {
            this.factorObservers = Arrays.asList(factorObservers);
            return;
        }
        this.factorObservers.addAll(Arrays.asList(factorObservers));
    }

    public void addObservers(List<FactorObserver<F>> factorObserverList) {
        if (factorObserverList == null) {
            return;
        }
        if (this.factorObservers == null) {
            this.factorObservers = new ArrayList<>(factorObserverList.size());
        }
        this.factorObservers.addAll(factorObserverList);
    }

    /**
     * 返回因子监听器
     * @return 因子监听器数组
     */
    protected FactorObserver<F>[] getFactorObservers() {
        if (factorObservers == null) {
            return null;
        }
        return factorObservers.toArray(new FactorObserver[] {});
    }

    /**
     * 生产因子
     * @return 因子
     */
    protected F produceFactor(FactorDescriber<F> describer) {
        if (describer == null) {
            return null;
        }

        // 1. 实例化，子类自主实现。
        F factor = instance();

        // 2. 属性填充
        FactorUtil.copyProperties(describer, factor);

        // 3. 初始化 （暂无）...

        return factor;
    }

    @Override
    public F get(Long factorId) {
        FactorDescriber<F> describer = factorVariableRepository.getDescribe(factorId);
        return produceFactor(describer);
    }

    /**
     * 创建因子
     * @param describer 因子描述
     * @return 创建的因子ID
     */
    public Long create(FactorDescriber<F> describer) {
        return factorVariableRepository.create(describer);
    }

    /**
     * 根据ID修改因子信息
     * @param describer 因子描述
     * @return 修改结果
     */
    public boolean updateById(FactorDescriber<F> describer) {
        return factorVariableRepository.updateById(describer);
    }

    /**
     * 根据ID删除因子
     * @param factorId
     * @return
     */
    public boolean deleteById(Long factorId) {
        return factorVariableRepository.deleteById(factorId);
    }

    /**
     * 自主实现每一个因子的实例化方法
     * @return 实例化后的因子
     */
    protected abstract F instance();

}
