package com.shop.criterion.factor.repository;

import com.shop.criterion.factor.Factor;

/**
 * 因子默认操作资源接口。<br>
 * 1. 用于获得因子描述器。
 * 2. 提交创建因子请求
 * @author JayEinstein
 */
public interface FactorVariableRepository<F extends Factor> extends FactorSource<F>, FactorHandle<F> {



}
