package com.shop.criterion.tools;

import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;

/**
 * Lambda操作工具
 * @author JayEinstein
 */
public abstract class LambdaUtil {

    /**
     * lambda类名+方法名
     * @param lambda lambda
     * @return 签名
     */
    public static String getLambdaSignature(Serializable lambda) {
        SerializedLambda serializedLambda = computeSerializedLambda(lambda);
        if (serializedLambda != null) {
            return serializedLambda.getImplClass() + "#" + serializedLambda.getImplMethodName();
        }
        return "";
    }

    /**
     * 获得Lambda方法名称
     * @param lambda Lambda表达式
     * @return Lambda方法名称
     */
    public static String getMethodName(Serializable lambda) {
        SerializedLambda serializedLambda = computeSerializedLambda(lambda);
        if (serializedLambda != null) {
            return serializedLambda.getImplMethodName();
        }
        return "";
    }

    private static SerializedLambda computeSerializedLambda(Serializable lambda) {
        Class<?> clz = lambda.getClass();

        try {
            Method m = clz.getDeclaredMethod("writeReplace");
            m.setAccessible(true);
            Object replacement = m.invoke(lambda);
            return replacement instanceof SerializedLambda ? (SerializedLambda)replacement : null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
