package com.shop.criterion.factor;

/**
 * 因素观察者
 * @author JayEinstein
 */
public interface FactorObserver<F extends Factor> {

    /**
     * 因子变化通知
     * @param spreadContext 传播上下文
     */
    void receive(SpreadContext<F> spreadContext);

}
