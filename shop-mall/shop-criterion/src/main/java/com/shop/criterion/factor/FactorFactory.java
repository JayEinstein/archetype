package com.shop.criterion.factor;

/**
 * 因子创建工厂。<br>
 * 对因子进行创建，遵循 实例化 -> 属性填充 -> 初始化 的过程。<br>
 * 为了保持因子功能的完整性。因子只能有FactorFactory进行创建。
 * @author JayEinstein
 */
public interface FactorFactory<F extends Factor> {

    /**
     * 获得因子
     * @param factorId 因子ID
     * @return 功能完整的因子
     */
    F get(Long factorId);

}
