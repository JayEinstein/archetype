package com.shop.criterion.func;

import com.shop.criterion.factor.Factor;

/**
 * 传播缓存因子
 * @author JayEinstein
 */
@FunctionalInterface
public interface SpreadFactorCache<F extends Factor> {

    /**
     * 获得缓存因子
     * @return 缓存因子
     */
    F get();

}
