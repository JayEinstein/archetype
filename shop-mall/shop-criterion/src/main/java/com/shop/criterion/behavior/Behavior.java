package com.shop.criterion.behavior;

/**
 * 行为接口<br>
 * 由于行为本身会携带单例的FactorFactory，
 * 所以行为子类必须也是单例、无状态的
 * @author JayEinstein
 */
public interface Behavior {
}
