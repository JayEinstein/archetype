package com.shop.criterion.factor;

import com.shop.criterion.factor.repository.FactorVariableRepository;

/**
 * 持久化因子。<br>
 * 拥有完整的因子信息
 * @author JayEinstein
 */
public abstract class PersistentFactor<R extends FactorVariableRepository> extends Factor {

    /**
     * 持久化因子资源通用接口
     */
    protected final R factorRepository;

    /**
     * 兼容@Data，默认私有构造方法
     */
    private PersistentFactor() {
        factorRepository = null;
    }

    /**
     * 持久因子构建
     * @param factorRepository 持久资源对象
     */
    protected PersistentFactor(R factorRepository) {
        this.factorRepository = factorRepository;
    }

}
