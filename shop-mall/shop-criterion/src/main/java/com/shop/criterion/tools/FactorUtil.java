package com.shop.criterion.tools;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.FactorDescriber;

import java.util.Collection;

/**
 * 因子常用操作封装
 * @author JayEinstein
 */
public abstract class FactorUtil {

    /**
     * 因子赋值<br>
     * 1. 空值不覆盖。当source x变量为null，而factor x变量为'abc'，则factor x变量依旧为'abc'
     * @param source 目标值对象
     * @param factor 赋值因子
     */
    public static void copyProperties(Object source, Factor factor) {

        if (isEmptyProperties(source)) {
            return;
        }

        copySourceProperties(source, factor);
    }

    /**
     * 描述赋值
     * @param source 目标值对象
     * @param factorDescriber 因子描述
     */
    public static void copyProperties(Object source, FactorDescriber factorDescriber) {
        copySourceProperties(source, factorDescriber);
    }

    private static void copySourceProperties(Object source, Object target) {
        CopyOptions copyOptions = CopyOptions.create(target.getClass(), true);
        BeanUtil.copyProperties(source, target, copyOptions);
    }

    public static <T> T copyProperties(Object source, Class<T> tClass) {
        if (source == null) {
            return null;
        }

        T t = ReflectUtil.newInstanceIfPossible(tClass);
        copySourceProperties(source, t);
        return t;
    }

    /**
     * 目标属性是否为空
     * @param bean 目标值对象
     * @return 是否为空
     */
    public static boolean isEmptyProperties(Object bean) {

        if (bean == null) {
            return true;
        }

        if (bean instanceof Collection) {
            return CollUtil.isEmpty((Collection<?>) bean);
        }

        return false;
    }

}
