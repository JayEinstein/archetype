package com.shop.criterion.factor;

import com.shop.criterion.func.SpreadFactorCache;
import com.shop.criterion.func.TypeFunction;
import com.shop.criterion.tools.LambdaUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 传播上下文
 * @author JayEinstein
 */
public class SpreadContext<F extends Factor> {

    /**
     * 传播发起的因子缓存
     */
    private SpreadFactorCache<F> spreadFactorCache;

    /**
     * 缓存因子
     */
    private F cacheFactor;

    /**
     * 因子工厂映射
     */
    private Map<Class<? extends Factor>, AbstractFactorFactory<?>> factoryMap = new HashMap<>();

    /**
     * lambda值映射
     */
    private Map<String, Object> funcMap = new HashMap<>();

    public <T, R> SpreadContext<F> put(TypeFunction<T, R> function, R r) {
        funcMap.put(LambdaUtil.getLambdaSignature(function), r);
        return this;
    }

    public <T, R> R get(TypeFunction<T, R> function) {
        Object val = funcMap.get(LambdaUtil.getLambdaSignature(function));
        if (val == null) {
            return null;
        }
        return (R) val;
    }

    /**
     * 放入因子缓存
     * @param spreadFactorCache 缓存因子接口
     * @return 传播上下文
     */
    public SpreadContext<F> putFactor(SpreadFactorCache<F> spreadFactorCache) {
        this.spreadFactorCache = spreadFactorCache;
        return this;
    }

    /**
     * 放入因子工厂
     * @param factorClass 因子类
     * @param factorFactory 因子工厂
     * @param <E> 因子类型
     * @return 传播上下文
     */
    public <E extends Factor> SpreadContext<F> putFactory(Class<E> factorClass, AbstractFactorFactory<E> factorFactory) {
        factoryMap.put(factorClass, factorFactory);
        return this;
    }

    /**
     * 获得因子工厂
     * @param factorClass 因子类
     * @param <E> 因子类型
     * @return 传播上下文
     */
    public <E extends Factor> AbstractFactorFactory<E> getFactory(Class<E> factorClass) {
        return (AbstractFactorFactory<E>) factoryMap.get(factorClass);
    }

    /**
     * 获得缓存因子
     * @return 传播上下文
     */
    public F getCacheFactor() {
        if (spreadFactorCache == null) {
            return null;
        }
        if (cacheFactor == null) {
            synchronized (this) {
                if (cacheFactor == null) {
                    cacheFactor = spreadFactorCache.get();
                }
            }
        }
        return cacheFactor;
    }

}

