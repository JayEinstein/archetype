package com.shop.criterion.factor;

import java.util.Arrays;
import java.util.List;

/**
 * 变量因素
 * @author JayEinstein
 */
public abstract class Factor {

    /**
     * 构造器私有化，只能通过{@link FactorFactory}进行构建
     */
    protected Factor() {}

    /**
     * 唯一id
     */
    protected Long id;

    /**
     * 因子观察者集合
     */
    protected List<FactorObserver<?>> observerList;

    /**
     * 增加观察者，只能通过{@link FactorFactory}进行添加，且只能添加一次
     * @param observers 观察者
     * @return 添加成功与否
     */
    protected <F extends Factor> boolean addObserver(FactorObserver<F>... observers) {
        if (observers == null) {
            return false;
        }
        if (this.observerList != null && !observerList.isEmpty()) {
            return false;
        }
        this.observerList = Arrays.asList(observers);
        return true;
    }

    /**
     * 通知所有观察者
     */
    protected void notifyObserver() {

        notifyObserver(null);

    }

    /**
     * 通知所属类型的观察者
     * @param clazz 通知类型
     */
    protected <F extends Factor, E extends FactorObserver<F>> void notifyObserver(Class<E> clazz) {
        // 传播上下文
        SpreadContext<F> spreadContext = generateSpreadContext();
        // 执行通知
        notifyObserver(clazz, spreadContext);
    }

    /**
     * 通知所属类型的观察者
     * @param clazz 通知类型
     * @param spreadContext 传播上下文
     */
    protected <F extends Factor, E extends FactorObserver<F>> void notifyObserver(Class<E> clazz, SpreadContext<F> spreadContext) {

        if (observerList == null) {
            return;
        }

        for (FactorObserver factoryObserver : observerList) {

            // 通知所有观察者
            if (clazz == null) {
                factoryObserver.receive(spreadContext);
                continue;
            }

            // 通知对应类型的观察者
            if (clazz.isAssignableFrom(factoryObserver.getClass())) {
                factoryObserver.receive(spreadContext);
                continue;
            }

        }
    }

    /**
     * 生成传播内容
     * @param <F> 传播因子类型
     * @return 传播上下文
     */
    protected <F extends Factor> SpreadContext<F> generateSpreadContext() {
        SpreadContext<F> spreadContext = new SpreadContext<>();
        // 持久因子
        if (this instanceof PersistentFactor) {
            spreadContext.putFactor(() -> (F) this);
        }
        // 回响因子
        if (this instanceof EchoFactor) {
            EchoFactor echoFactor = (EchoFactor) this;
            Class parentClass = echoFactor.getParentClass();
            spreadContext.putFactory(parentClass, echoFactor.parentFactory);
        }
        return spreadContext;
    }

    /**
     * 不对外开放
     * @return
     */
    protected List<FactorObserver<?>> getObserverList() {
        return observerList;
    }

    /**
     * 不对外开放
     * @return
     */
    protected void setObserverList(List<FactorObserver<?>> observerList) {
        this.observerList = observerList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
