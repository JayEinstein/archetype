package com.shop.criterion.behavior;

import com.shop.criterion.factor.Factor;

import java.util.HashMap;
import java.util.Map;

/**
 * 行为的入参对象
 * @author JayEinstein
 */
public class BehaviorMap {

    // O -> Bo

    // 会话单例，会话上下文，因子会话单例

    private final Map<Class<? extends Factor>, Factor> factorMap = new HashMap<>();

    public BehaviorMap putFactor(Factor... factors) {

        for (Factor factor : factors) {
            factorMap.put(factor.getClass(), factor);
        }

        return this;
    }

    public <F extends Factor> F getFactor(Class<F> factorClass) {
        return (F) factorMap.get(factorClass);
    }


}
