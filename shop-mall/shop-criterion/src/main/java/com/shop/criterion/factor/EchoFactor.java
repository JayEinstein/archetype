package com.shop.criterion.factor;

import com.shop.criterion.factor.repository.FactorEcho;

/**
 * 回响型因子。<br>
 * 根据部分的因子信息对因子进行操作。<br>
 * 回响因子是没有完整信息和状态的。
 * @param <F> 公因子
 * @author JayEinstein
 */
public abstract class EchoFactor<F extends PersistentFactor> extends Factor {

    /**
     * 回响接口
     */
    protected final FactorEcho factorEcho;

    /**
     * 父类公因子工厂
     */
    protected final AbstractFactorFactory<F> parentFactory;

    protected EchoFactor(AbstractFactorFactory<F> parentFactory, FactorEcho factorEcho) {
        this.parentFactory = parentFactory;
        this.factorEcho = factorEcho;
    }

    /**
     * 获得公因子类型
     * @return
     */
    public abstract Class<F> getParentClass();

}
