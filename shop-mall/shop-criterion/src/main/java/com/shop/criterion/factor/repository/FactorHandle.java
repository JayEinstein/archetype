package com.shop.criterion.factor.repository;

import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.FactorDescriber;

/**
 * 因子手柄。<br>
 * 主观上直接对因子进行修改的行为。(增，删，改)
 * @author JayEinstein
 */
public interface FactorHandle<F extends Factor> extends FactorEcho {

    /**
     * 根据描述创建因子
     * @param describer 因子的描述
     * @return 因子ID
     */
    Long create(FactorDescriber<F> describer);

    /**
     * 根据因子ID修改因子信息
     * @param describer 因子的描述
     * @return 修改成果与否
     */
    boolean updateById(FactorDescriber<F> describer);

    /**
     * 根据ID删除因子。默认不实现
     * @param factorId 因子ID
     * @return 删除与否
     */
    default boolean deleteById(Long factorId) {
        return false;
    }

}
