package com.shop.criterion.factor.repository;

import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.FactorDescriber;

/**
 * 因子资源接口。<br>
 * 这类型的接口，划分的返回类型都是 {@link FactorDescriber<F>}
 * @author JayEinstein
 */
public interface FactorSource<F extends Factor> extends FactorRepository {

    /**
     * 因子描述
     * @param factorId
     * @return
     */
    FactorDescriber<F> getDescribe(Long factorId);

}
