package com.shop.criterion.factor;

import com.shop.criterion.tools.FactorUtil;

/**
 * 因子描述器。是Factor和FactorRepository交流的协议。
 * FactorRepository返回FactorDescriber进行因子构建。<br>
 * Factor传输出FactorDescriber进行创建，修改。<br>
 * @author JayEinstein
 */
public abstract class FactorDescriber<F extends Factor> {

    /**
     * 因子ID
     */
    protected Long id;

    private FactorDescriber() {
        this(null);
    }

    public FactorDescriber(Object value) {
        if (value == null) {
            return;
        }
        FactorUtil.copyProperties(value, this);
    }

    public FactorDescriber<F> id(Long factorId) {
        this.id = factorId;
        return this;
    }

    /**
     * 检查新增描述是否正确。错误的话抛出异常
     */
    public void checkInsert() {}

    /**
     * 检查修改操作描述是否正确。错误的话抛出异常
     */
    public void checkUpdate() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 把值转化为因子描述
     * @param sources 值对应
     * @return 因子对象
     */
    public FactorDescriber<F> ofValue(Object... sources) {
        for (Object source : sources) {
            FactorUtil.copyProperties(source, this);
        }
        return this;
    }

    /**
     * 转换对象
     * @param tClass 目标对象
     * @param <T> 目标类型
     * @return 目标值
     */
    public <T> T convert(Class<T> tClass) {
        if (Factor.class.isAssignableFrom(tClass)) {
            throw new RuntimeException("不能转换为Factor类型");
        }
        return FactorUtil.copyProperties(this, tClass);
    }

}
