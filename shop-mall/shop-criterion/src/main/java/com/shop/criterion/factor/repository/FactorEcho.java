package com.shop.criterion.factor.repository;

/**
 * 因子回响。<br>
 * 本质上，所有的程序都是要有响应的，这里基于这个前提下定义回响。(人人都练葵花bd，那么就等于人人都没有练葵花bd...)<br>
 * 1. 对因子进行了**操作**，并获得响应的行为，称之为”因子回响“。<br>
 * 只是查看因子信息的行为不划分到这一目录下，可以另立分类。<br>
 * 2. 常用于：只用有限的信息去改变因子状态，并获得状态改变响应的操作。<br>
 * 3. 主观上直接对因子进行修改的行为，使用{@link FactorHandle}。<br>
 * @author JayEinstein
 */
public interface FactorEcho extends FactorRepository {


}
