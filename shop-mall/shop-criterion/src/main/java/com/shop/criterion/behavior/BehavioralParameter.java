package com.shop.criterion.behavior;

import java.util.Map;

/**
 * 行为参数
 * @author JayEinstein
 */
public interface BehavioralParameter<K, V> extends Map<K, V> {
}
