package test.com.shop.criterion.tools;

import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.SpreadContext;
import com.shop.criterion.func.TypeFunction;
import com.shop.criterion.tools.LambdaUtil;
import lombok.Data;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author JayEinstein
 */
public class LambdaUtilTest {

    @Test
    public void test02() {

        SpreadContext<Factor> context = new SpreadContext<>();

        context.put(LambdaVo::getName, "一个名字")
               .put(LambdaVo::getCount, 6)
               .put(LambdaVo::getPrice, BigDecimal.TEN)
               .put(LambdaVo::getLs, Arrays.asList("a", "b", "c"))
        ;

        String str = context.get(LambdaVo::getName);
        System.out.println(str);

        Integer integer = context.get(LambdaVo::getCount);
        System.out.println(integer);

        BigDecimal bigDecimal = context.get(LambdaVo::getPrice);
        System.out.println(bigDecimal);

        List<String> strings = context.get(LambdaVo::getLs);
        System.out.println(strings);

    }

    @Test
    public void test01() {

        String func = func(LambdaVo::getName);
        System.out.println(func);

        String lambda = lambda(LambdaVo::getName, "名字");
        System.out.println(lambda);

    }

    private <T, R> String func(TypeFunction<T, R> func) {

        return LambdaUtil.getMethodName(func);
    }

    private <T, R> String lambda(TypeFunction<T, R> function, R r) {

        String lambdaFieldName = LambdaUtil.getMethodName(function);

        System.out.println(r);

        return lambdaFieldName;
    }

}

@Data
class LambdaVo {

    private String name;

    private BigDecimal price;

    private Integer count;

    private List<String> ls;

}
