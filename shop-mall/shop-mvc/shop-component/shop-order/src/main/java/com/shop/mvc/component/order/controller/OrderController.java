package com.shop.mvc.component.order.controller;

import com.shop.behavior.core.PaymentBehavior;
import com.shop.behavior.core.dto.CreateOrderDto;
import com.shop.factor.order.Order;
import com.shop.factor.order.OrderDescriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author JayEinstein
 */
@RestController
@RequestMapping("/shop/order")
public class OrderController {

    @Autowired
    private PaymentBehavior paymentBehavior;

    /**
     * 创建订单
     * @param createOrder
     * @return
     */
    @PostMapping("/create")
    public OrderDescriber createOrder(@RequestBody CreateOrderDto createOrder) {

        OrderDescriber orderDescriber = paymentBehavior.createOrder(createOrder);

        return orderDescriber;
    }

    /**
     * 支付成功回调
     * @param orderId
     * @return
     */
    @GetMapping("/paid")
    public Object paidSuccess(Long orderId) {

        String res = paymentBehavior.paidSuccess(orderId);

        return res;

    }

}
