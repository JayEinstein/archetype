package com.shop.mvc.component.product.controller;

import com.shop.factor.product.Product;
import com.shop.factor.product.ProductFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JayEinstein
 */
@RestController
@RequestMapping("/shop/product")
public class ProductController {

    @Autowired
    private ProductFactory productFactory;

    /**
     * 获得产品信息
     * @param productId
     * @return
     */
    @GetMapping("/get")
    public Object getProduct(Long productId) {
        Product product = productFactory.get(productId);
        System.out.println("产品服务获得产品信息，" + product);
        return product;
    }

}
