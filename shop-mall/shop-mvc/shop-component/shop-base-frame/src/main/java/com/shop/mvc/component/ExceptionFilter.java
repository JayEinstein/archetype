package com.shop.mvc.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author JayEinstein
 */
@RestControllerAdvice
@Slf4j
public class ExceptionFilter {

    /**
     * 异常处理
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public Object handleException(Exception ex) {

        log.error("程序出现异常", ex);

        return ex.getMessage();
    }

}
