package com.shop.mvc.behavior.register;

import com.shop.behavior.core.PaymentBehavior;
import com.shop.factor.member.MemberFactory;
import com.shop.factor.order.OrderFactory;
import com.shop.factor.product.ProductFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 行为注册
 * @author JayEinstein
 */
@Configuration
public class BehaviorRegister {

    /**
     * 注册行为
     * @param productFactory 产品构造器
     * @param orderFactory 订单构造器
     * @param memberFactory 会员构造器
     * @return
     */
    @Bean
    public PaymentBehavior paymentBehavior(
            ProductFactory productFactory,
            OrderFactory orderFactory,
            MemberFactory memberFactory
    ) {

        return new PaymentBehavior(productFactory, orderFactory, memberFactory);
    }

}
