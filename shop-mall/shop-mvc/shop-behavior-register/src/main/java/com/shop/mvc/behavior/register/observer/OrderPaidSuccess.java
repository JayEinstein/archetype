package com.shop.mvc.behavior.register.observer;

import com.shop.criterion.factor.Factor;
import com.shop.criterion.factor.SpreadContext;
import com.shop.factor.order.Order;
import com.shop.factor.order.subject.OrderPaidObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 订单支付成功通知
 * @author JayEinstein
 */
@Component
@Slf4j
public class OrderPaidSuccess implements OrderPaidObserver {

    @Override
    public void receive(SpreadContext<Order> spreadContext) {
        Order order = spreadContext.getCacheFactor();
        log.info("订单编号：{} 支付成功", order.getOrderCode());
    }
}
