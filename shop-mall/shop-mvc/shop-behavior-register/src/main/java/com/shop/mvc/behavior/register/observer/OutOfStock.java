package com.shop.mvc.behavior.register.observer;

import com.shop.criterion.factor.SpreadContext;
import com.shop.factor.product.Product;
import com.shop.factor.product.subject.OutOfStockObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 产品售罄通知
 * @author JayEinstein
 */
@Component
@Slf4j
public class OutOfStock implements OutOfStockObserver {


    @Override
    public void receive(SpreadContext<Product> spreadContext) {
        Product product = spreadContext.getCacheFactor();
        log.info("产品售罄通知：{} 已售罄", product.getName());
    }

}
