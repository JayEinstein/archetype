package com.shop.mvc.behavior.register;

import com.shop.criterion.factor.FactorObserver;
import com.shop.factor.member.MemberFactory;
import com.shop.factor.member.respository.MemberRepository;
import com.shop.factor.order.Order;
import com.shop.factor.order.OrderFactory;
import com.shop.factor.order.repository.OrderRepository;
import com.shop.factor.product.Product;
import com.shop.factor.product.ProductFactory;
import com.shop.factor.product.repository.ProductRepository;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;

import java.util.List;

/**
 * 组件内因子工厂注册
 * @author JayEinstein
 */
@Configuration(proxyBeanMethods = false)
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class FactorFactoryRegister {

    @Bean
    public OrderFactory orderBuilder(OrderRepository orderRepository,
                                         List<FactorObserver<Order>> observers) {

        OrderFactory orderFactory = new OrderFactory(orderRepository);
        orderFactory.addObservers(observers);

        return orderFactory;
    }

    @Bean
    public ProductFactory productBuilder(ProductRepository productRepository,
                                             List<FactorObserver<Product>> observers) {

        ProductFactory productFactory = new ProductFactory(productRepository);
        productFactory.addObservers(observers);
        return productFactory;
    }

    @Bean
    public MemberFactory memberBuilder(MemberRepository memberRepository) {
        return new MemberFactory(memberRepository);
    }

}
