package com.shop.mvc.dao.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * mysql数据表实体类
 * @author JayEinstein
 */
@Data
@TableName("shop_member")
public class MemberEntity {

    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 用户名称
     */
    private String userName;

}
