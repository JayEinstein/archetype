package com.shop.mvc.dao.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

/**
 * mysql数据表实体类
 * @author JayEinstein
 */
@Data
@TableName("shop_order")
public class OrderEntity {

    /**
     * id
     */
    @TableId
    private Long id;

    /**
     * 订单编码
     */
    private String orderCode;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品编码
     */
    private String code;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 产品价格
     */
    private BigDecimal price;

    /**
     * 订单状态
     */
    private String status;

    /**
     * 订单支付金额
     */
    private BigDecimal payAmount;

}
