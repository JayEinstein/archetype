package com.shop.mvc.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.mvc.dao.entity.OrderEntity;

/**
 * @author JayEinstein
 */
public interface OrderEntityMapper extends BaseMapper<OrderEntity> {
}
