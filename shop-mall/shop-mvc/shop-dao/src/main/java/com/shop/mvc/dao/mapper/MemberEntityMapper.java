package com.shop.mvc.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.mvc.dao.entity.MemberEntity;

/**
 * @author JayEinstein
 */
public interface MemberEntityMapper extends BaseMapper<MemberEntity> {
}
