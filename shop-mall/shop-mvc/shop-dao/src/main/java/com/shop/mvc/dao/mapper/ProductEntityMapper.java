package com.shop.mvc.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.mvc.dao.entity.ProductEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

/**
 * @author JayEinstein
 */
public interface ProductEntityMapper extends BaseMapper<ProductEntity> {

    /**
     * 扣减库存
     * @param num 扣减数量
     * @return
     */
    @Insert("update shop_product set stock = stock - #{num} where id = #{id} and stock >= #{num}")
    boolean deductionStock(@Param("id") Long id,@Param("num") Integer num);

    /**
     * 增加库存
     * @param num 增加的数量
     * @return
     */
    @Insert("update shop_product set stock = stock + #{num} where id = #{id}")
    boolean increaseStock(@Param("id") Long id,@Param("num") Integer num);

}
