package com.shop.behavior.core;


import com.shop.behavior.core.dto.CreateOrderDto;
import com.shop.criterion.behavior.Behavior;
import com.shop.factor.member.Member;
import com.shop.factor.member.MemberFactory;
import com.shop.factor.order.*;
import com.shop.factor.product.Product;
import com.shop.factor.product.ProductFactory;

import java.math.BigDecimal;

/**
 * 支付行为标准化
 * @author JayEinstein
 */
public class PaymentBehavior implements Behavior {

    /**
     * 产品工厂
     */
    private final ProductFactory productFactory;

    /**
     * 订单工厂
     */
    private final OrderFactory orderFactory;

    /**
     * 会员工厂
     */
    private final MemberFactory memberFactory;

    public PaymentBehavior(ProductFactory productFactory,
                           OrderFactory orderFactory,
                           MemberFactory memberFactory) {
        this.productFactory = productFactory;
        this.orderFactory = orderFactory;
        this.memberFactory = memberFactory;
    }

    /**
     * 简单的下单行为封装
     * @param createDto
     */
    public OrderDescriber createOrder(CreateOrderDto createDto) {

        /* 1. 参数合法性校验 */
        Long memId = createDto.getMemId();
        if (memId == null) {
            throw new RuntimeException("会员Id不能为空");
        }
        Long productId = createDto.getProductId();
        if (productId == null) {
            throw new RuntimeException("购买的产品Id不能为空");
        }
        Integer buyNum = createDto.getBuyNum();
        if (buyNum == null) {
            throw new RuntimeException("购买的数量不能为空");
        }

        /* 2. 因子合法性校验 */
        Member member = memberFactory.get(memId);
        if (member == null) {
            throw new RuntimeException(String.format("会员信息错误，%s", memId));
        }

        Product product = productFactory.get(productId);
        if (product == null) {
            throw new RuntimeException("购买的产品不存在");
        }

        /* 3. 因子操作 */
        // 预扣库存
        boolean res = product.deductionStock(buyNum);
        if (!res) {
            throw new RuntimeException(String.format("%s库存不足", product.getName()));
        }

        /* 4. 校验通过，创建订单 */
        OrderDescriber orderDescriber = new OrderDescriber();
        orderDescriber.setStatus(OrderStatusEnum.WAIT)
                .setOrderCode(System.currentTimeMillis() + "")
                .ofValue(product).setId(null);

        /* 5. 计算订单价格 */
        BigDecimal payAmount = product.getPrice().multiply(BigDecimal.valueOf(buyNum));
        orderDescriber.setPayAmount(payAmount);

        try {
            // 持久化订单
            Long orderId = orderFactory.create(orderDescriber);

            orderDescriber.setId(orderId);

            if (orderId == null) {
                throw new RuntimeException("创建订单失败");
            }
        } catch (Exception e) {
            // 返回库存
            product.increaseStock(buyNum);
            throw e;
        }

        /* 6. 订单创建成功 */
        return orderDescriber;
    }

    /**
     * 订单支付成功
     * @param orderId 订单Id
     */
    public String paidSuccess(Long orderId) {

        // 获得订单回响
        OrderEcho orderEcho = orderFactory.getOrderEcho();

        // 修改支付状态
        boolean res = orderEcho.toPaid(orderId);

        return res ? "支付成功" : "支付失败";
    }

}
