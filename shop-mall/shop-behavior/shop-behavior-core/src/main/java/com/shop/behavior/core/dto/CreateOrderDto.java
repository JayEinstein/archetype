package com.shop.behavior.core.dto;

import lombok.Data;

/**
 * 创建订单
 * @author JayEinstein
 */
@Data
public class CreateOrderDto {

    /**
     * 会员ID
     */
    private Long memId;

    /**
     * 构建的产品ID
     */
    private Long productId;

    /**
     * 购买数量
     */
    private Integer buyNum;

}
